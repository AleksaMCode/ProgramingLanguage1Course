#include "Library.h"

void Library::copy(const Library& other)
{
	parray = new Member*[other.capacity];
	name = new char[strlen(other.name)];
	std::copy(other.parray, other.parray + numberOfMembers, parray);
}

inline void Library::move(Library&& other)
{
	parray = other.parray;
	name = other.name;
	capacity = other.capacity;
	numberOfMembers = other.numberOfMembers;
	other.parray = nullptr;
	other.name = nullptr;
}

Library::Library(string name, int capacity) noexcept : name(name), capacity(capacity), parray(new Member*[capacity])
{
	std::fill(parray, parray + capacity, nullptr);
}

Library::Library(const Library& other)
{
	copy(other);
}

Library::Library(Library&& other) noexcept
{
	move(std::move(other));
}

Library::~Library()
{
	for (int i = 0; i < numberOfMembers; ++i)
		delete parray[i]; // problem ?

	delete[] parray;
	delete[] name;
	capacity = numberOfMembers = 0;
	parray = nullptr;
}

int Library::operator()() noexcept { return numberOfMembers; }

void Library::operator+=(Member& element) noexcept(false)
{
	try
	{
		if (numberOfMembers == capacity)
			throw std::overflow_error("Overflow Error.");
		parray[numberOfMembers++] = &element;
	}
	catch (std::exception ex) { std::cout << ex.what(); }
}

void Library::operator++() noexcept(false)
{
	Member** carr = new Member*[capacity *= 2];
	std::copy(parray, parray + numberOfMembers, carr);
	delete[] parray;
	parray = new Member*[capacity];
	parray = carr;
	carr = nullptr;
}

Member& Library::operator[](int index) noexcept(false)
{
	if (index < 0 || index > numberOfMembers)
		throw std::out_of_range("Index is out of range.");
	else
		return *parray[index];
}

const Member& Library::operator[](int index) const noexcept(false)
{
	return const_cast<Member&>(const_cast<Library&>(*this)[index]);
}

Library& Library::operator=(const Library& other)
{
	if (this != &other)
	{
		Library::~Library();
		copy(other);
	}

	return *this;
}

Library& Library::operator=(Library&& other) noexcept
{
	if (this != &other)
	{
		Library::~Library();
		move(std::move(other));
	}

	return *this;
}

void Library::print(std::ostream& out) const noexcept
{
	out << "NAME:" << name << std::endl;
	for (int i = 0; i < numberOfMembers; ++i)
		out << *parray[i] << std::endl;
}

std::ostream& operator<<(std::ostream& out, const Library& lib) noexcept
{
	lib.print(out);
	return out;
}

// Code by: Aleksa M. 