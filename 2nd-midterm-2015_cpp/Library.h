#pragma once
#include "Member.h"
#include <algorithm>
#include <exception>
#include <stdexcept>

class Library
{
	Member** parray;
	string name;
	int capacity, numberOfMembers = 0;
	void copy(const Library&);
	inline void move(Library&&);
	friend std::ostream& operator<<(std::ostream&, const Library&) noexcept;
public:
	Library(string, int capacity = 10) noexcept;
	Library(const Library&);
	Library(Library&&) noexcept;
	~Library();
	int operator()() noexcept;
	void operator+=(Member&) noexcept(false);
	void operator++() noexcept(false);
	Member& operator[](int) noexcept(false);
	const Member& operator[](int) const noexcept(false);
	Library& operator=(const Library&);
	Library& operator=(Library&&) noexcept;
	virtual void print(std::ostream&) const noexcept;
};

// Code by: Aleksa M. 