#include "Member.h"

void Member::copy(const Member& other)
{
	fname = new char[strlen(other.fname) + 1];
	lname = new char[strlen(other.lname) + 1];
	strcpy(fname, other.fname);
	strcpy(lname, other.lname);
}

inline void Member::move(Member&& other)
{
	fname = other.fname;
	lname = other.lname;
	other.fname = other.lname = nullptr;
}

Member::Member(string fname, string lname) : fname(new char[strlen(fname) + 1]), lname(new char[strlen(lname) + 1])
{
	strcpy(this->fname, fname);
	strcpy(this->lname, lname);
}

Member::Member(const Member& other)
{
	copy(other);
}

Member::Member(Member&& other) noexcept
{
	move(std::move(other));
}

Member& Member::operator=(const Member& other)
{
	if (this != &other)
	{
		delete[] fname;
		delete[] lname;
		copy(other);
	}
	return *this;
}

Member& Member::operator=(Member&& other) noexcept
{
	if (this != &other)
	{
		delete[] fname;
		delete[] lname;
		move(std::move(other));
	}
	return *this;
}

std::ostream& operator<<(std::ostream& out, const Member& other) noexcept
{
	other.print(out);
	return out;
}

void Member::print(std::ostream& out) const noexcept
{
	out << fname << " " << lname;
}

// Code by: Aleksa M. 