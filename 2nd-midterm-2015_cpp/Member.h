#pragma once
#include <iostream>
#include <string>
typedef char* string;

class Member
{
	string fname, lname;
	friend std::ostream& operator<<(std::ostream&, const Member&) noexcept;
	void copy(const Member&);
	inline void move(Member&&);
public:
	Member(string, string);
	Member(const Member&);
	Member(Member&&) noexcept;
	Member& operator=(const Member&);
	Member& operator=(Member&&) noexcept;
	virtual void print(std::ostream&) const noexcept;
};

// Code by: Aleksa M. 