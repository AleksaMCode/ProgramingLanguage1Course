#include "Library.h"
#include "Teacher.h"
#include "Student.h"
#include <cctype>
#include <fstream>
using ::std::cout;
using ::std::cin;

void setNames(string fname, string lname)
{
	cout << "First name: ", cin >> fname;
	cout << "Last name: ", cin >> lname;
}

int main()
{
	char name[50];
	cout << "Library name: ";
	cin >> name;
	Library lib(name);
	char c;
	do
	{
		system("PAUSE"), system("CLS");
		cout << "Student[S/s], Teacher[T/t], Exit[x/X]: ";
		cin >> c;
		char fname[20], lname[20];
		if (tolower(c) != 'x')
		{
			if (tolower(c) == 's')
			{
				setNames(fname, lname);
				char index[10];
				cout << "Index number: ", cin >> index;
				lib += *new Student(fname, lname, index);
			}
			else if (tolower(c) == 't')
			{
				setNames(fname, lname);
				int num;
				cout << "Office number: ", cin >> num;
				lib += *new Teacher(fname, lname, num);
			}
		}

	} while (tolower(c) != 'x');

	cout << lib;
	std::ofstream myfile;
	myfile.open("library.txt");
	myfile << lib;
	myfile.close();
}

// Code by: Aleksa M. 