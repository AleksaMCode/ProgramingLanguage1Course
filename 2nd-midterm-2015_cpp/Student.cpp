#include "Student.h"

void Student::print(std::ostream& out) const noexcept
{
	Member::print(out);
	out << " " << indexNumber;
}

Student::Student(string fname, string lname, char* indexNumber) : Member(fname, lname) { strcpy(this->indexNumber, indexNumber); }

// Code by: Aleksa M. 