#pragma once
#include "Member.h"
#include <cstring>

class Student : virtual public Member
{
	char indexNumber[10];
public:
	Student(string, string, char*);
	virtual void print(std::ostream&) const noexcept override;
};

// Code by: Aleksa M. 