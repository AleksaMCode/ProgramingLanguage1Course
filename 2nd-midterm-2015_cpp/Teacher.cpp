#include "Teacher.h"

void Teacher::print(std::ostream& out) const noexcept 
{
	Member::print(out);
	out << " " << officeNumber;
}

Teacher::Teacher(string fname, string lname, int officeNumber) : Member(fname,lname), officeNumber(officeNumber) {}

// Code by: Aleksa M. 