#pragma once
#include "Member.h"

class Teacher : virtual public Member
{
	int officeNumber;
public:
	Teacher(string, string, int);
	virtual void print(std::ostream&) const noexcept override;
};

// Code by: Aleksa M. 