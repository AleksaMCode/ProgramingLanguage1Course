#pragma once
#include "ItemHeader.h"

template<typename T>
Item<T>::Item(const T& item) noexcept : item(item)
{
	std::cout << *this;
}

template<typename T>
void Item<T>::print(std::ostream& out) const noexcept
{
	out << item;
}

// Code by: Aleksa M. 