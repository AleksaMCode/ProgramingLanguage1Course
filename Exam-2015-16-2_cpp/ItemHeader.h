#pragma once
#include "Treasure.h"

template<typename T>
class Item : public Treasure
{
	T item;
public:
	Item(const T&) noexcept;
	// Item() = default;
	virtual void print(std::ostream&) const noexcept override;
};

// Code by: Aleksa M. 