#include "Item.h"
#include "Structure.h"
#include "Treasury.h"
#include <fstream>

int main()
{
	// 4th task
	Structure str1(2, 20.0), str2(3, 10.0);
	Item<int> item1(64);
	Item<double> item2(3.14);
	Item<char*> item3("Gold");
	Treasury tr1(item1), tr2(item2), tr3(item3);
	str1.add(tr1);
	str2.add(tr1);
	str2.add(tr2);
	str2.add(tr3);
	// predmete kastovati u trezore pri slanju argumenata konstruktoru ??
	std::ofstream myfile;
	myfile.open("exit.txt");
	myfile << "Structure 1 (free surface " << str1() << "):\n" << str1 << 
		"Structure 2 (free surface " << str2() << "):\n" << str2;
	str1.add(str2);
	myfile << "Structure 1 with Structure 2 added (free surface " << str1() << "):\n" << str1;
	try
	{
		str2.add(tr1);
	}
	catch (const char* str)
	{
		std::cout << str;
		myfile << "Add to Structure 2:\n" << str;
	}
	myfile.close();
}

// Code by: Aleksa M. 