#include "Structure.h"

void Structure::copy(const Structure& other)
{
	pArray = new Structure*[n = other.n];
	for (int i = 0; i < n; ++i)
		pArray[i] = other.pArray[i];
	surface = other.surface;
	takenSurface = other.takenSurface;
	numberOfStructures = other.numberOfStructures;
}

void Structure::move(Structure&& other)
{
	pArray = other.pArray;
	n = other.n;
	numberOfStructures = other.numberOfStructures;
	surface = other.surface;
	takenSurface = other.takenSurface;
	other.pArray = nullptr;
}

Structure::Structure(int n, double surface) noexcept(false) : n(n), surface(surface)
{
	pArray = new Structure*[n];
	std::fill(pArray, pArray + n, nullptr);
}

Structure::Structure(const Structure& other)
{
	copy(other);
}

Structure::Structure(Structure&& other) noexcept
{
	move(std::move(other));
}

Structure::~Structure()
{
	for (int i = 0; i < n; ++i)
		delete pArray[i]; // problem ?

	delete[] pArray;
	numberOfStructures =  n = 0;
	takenSurface = surface = 0.0;
	pArray = nullptr;
}

Structure& Structure::operator=(const Structure& other)
{
	if (this != &other)
	{
		Structure::~Structure();
		copy(other);
	}
	return *this;
}

Structure& Structure::operator=(Structure&& other) noexcept
{
	if (this != &other)
	{
		Structure::~Structure();
		move(std::move(other));
	}
	return *this;
}

void Structure::add(Structure& str) noexcept(false)
{
	if (numberOfStructures < n)
	{
		try
		{
			if (str.surface + takenSurface > surface)
				throw "Structure is too large.\n";
			pArray[numberOfStructures++] = &str;
			takenSurface += str.surface;
		}
		catch (const char* string) { std::cout << string; }
	}
	else throw "Structure is full.\n";
}

double Structure::operator()(void) noexcept
{
	return surface - takenSurface;
}

void Structure::print(std::ostream& out) const noexcept
{
	using std::endl;
	for (int i = 0; i < numberOfStructures; ++i)
		out << *pArray[i] << " ";
	out << endl;
}

Structure& Structure::copy_structure()
{
	Structure* temp = new Structure;
	temp = this;
	return *this;
}

std::ostream& operator<<(std::ostream& out, const Structure& str)
{
	str.print(out);
	return out;
}

// Code by: Aleksa M. 