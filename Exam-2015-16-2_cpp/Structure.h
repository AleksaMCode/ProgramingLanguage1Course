#pragma once
#include <iostream>
#include <algorithm>

class Structure
{
	int n = 0, // number of max structures
		numberOfStructures = 0;
	Structure** pArray;
	void copy(const Structure&);
	inline void move(Structure&&);
	friend std::ostream& operator<<(std::ostream&, const Structure&);
public:
	Structure(int = 10, double = 50.0) noexcept(false);
	Structure(const Structure&);
	Structure(Structure&&) noexcept;
	virtual ~Structure();
	Structure& operator=(const Structure&);
	Structure& operator=(Structure&&) noexcept;
	void add(Structure&) noexcept(false);
	double operator()(void) noexcept;
	virtual Structure& copy_structure();
protected:
	double surface = 0.0,
		takenSurface = 0.0;
	virtual void print(std::ostream&) const noexcept;
};

// Code by: Aleksa M. 