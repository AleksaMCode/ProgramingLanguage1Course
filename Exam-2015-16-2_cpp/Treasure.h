#pragma once
#include <iostream>

class Treasure
{
	friend std::ostream& operator<<(std::ostream&, const Treasure&);
public:
	// operator Treasury(); ??
	virtual void print(std::ostream&) const noexcept = 0;
};

// Code by: Aleksa M. 