#include "Treasury.h"

Treasury::Treasury(Treasure& obj) : obj(obj)
{
	surface = takenSurface = 2.0; // free surface = 0
}

Structure& Treasury::copy_structure()
{
	Treasury* temp = new Treasury(obj);
	return *this;
}

void Treasury::print(std::ostream& out) const noexcept
{
	out << obj << "\t";
}

// Code by: Aleksa M. 