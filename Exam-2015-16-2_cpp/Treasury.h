#pragma once
#include "Structure.h"
#include "Treasure.h"

class Treasury : public Structure
{
	Treasure& obj;
public:
	Treasury(Treasure&);
	virtual Structure& copy_structure() override;
	virtual void print(std::ostream&) const noexcept override;
};

// Code by: Aleksa M. 