#include "Cart.h"

void Cart::copy(const Cart& other)
{
	array = new ItemQuantity(numOfElements = other.numOfElements);
	std::copy(other.array, other.array + numOfElements, array);
}

inline void Cart::move(Cart&& other)
{
	array = other.array;
	numOfElements = other.numOfElements;
	other.array = nullptr;
}

Cart::Cart() : numOfElements(0) { array = new ItemQuantity[1]; }

Cart::Cart(const Cart& other) { copy(other); }

Cart::Cart(Cart&& other) { move(std::move(other)); }

Cart::~Cart()
{
	delete[] array;
	array = nullptr;
	numOfElements = 0;
}

void Cart::operator()(const Item& it, double q)
{
	if (numOfElements) {
		bool check = false;
		for (int i = 0; i < numOfElements; ++i)
			if (array[i].getArt() == it) array[i].getQuantity()++, check = 1, i = numOfElements;


		if (check == false)
		{
			ItemQuantity *temp = new ItemQuantity[numOfElements];
			std::copy(array, array + numOfElements, temp);
			delete[] array;
			array = new ItemQuantity[numOfElements + 1];
			std::copy(temp, temp + numOfElements, array);
			delete[] temp;
			array[numOfElements].getArt() = it;
			array[numOfElements].getQuantity() = q;
			numOfElements++;
		}
	}
	else
	{
		delete[] array;
		array = new ItemQuantity[1];
		array[numOfElements].getArt() = it;
		array[numOfElements].getQuantity() = q;
		numOfElements++;
	}

}

Cart& Cart::operator=(const Cart& other)
{
	if (this != &other) {
		delete[] array;
		copy(other);
	}
	return *this;
}

Cart& Cart::operator=(Cart&& other)
{
	if (this == &other)
		return *this;

	delete[] array;
	move(std::move(other));
	return *this;
}

double Cart::operator()(void) const
{
	double total(0);
	std::for_each(array, array + numOfElements, [&total](const ItemQuantity& el) { total += el(); });
	return total;
}

void Cart::operator--(int k)
{
	for (int i = 0; i < numOfElements; ++i)
		if (array[i].getQuantity() == k)
			minusOperatorHelp(i), i--;
}

void Cart::minusOperatorHelp(int index)
{
	ItemQuantity *temp = new ItemQuantity[numOfElements - 1];
	if (index)
	{
		std::copy(array, array + index, temp);
		std::copy(array + index + 1, array + numOfElements, temp + index);
	}
	else std::copy(array + 1, array + numOfElements, temp);

	delete[] array;
	array = new ItemQuantity[--numOfElements];
	std::copy(temp, temp + numOfElements, array);
	delete[] temp;
}

int Cart::getNumOfElements() const { return numOfElements; }

std::ostream& operator<<(std::ostream& out, const Cart& cart)
{
	if (cart.numOfElements) {
		std::for_each(cart.array, cart.array + cart.numOfElements, [&out](const ItemQuantity& el) {out << el << std::endl; });
	}
	else out << "Your cart is empty!" << std::endl;
	return out;
}

//Code by: Aleksa M. 