#pragma once
#include "ItemQuantity.h"
#include <algorithm>

class Cart
{
	ItemQuantity *array;
	int numOfElements;
	void copy(const Cart&);
	inline void move(Cart&&);
	friend std::ostream& operator<<(std::ostream&, const Cart&);
public:
	Cart();
	Cart(const Cart&);
	Cart(Cart&&);
	~Cart();
	void operator()(const Item&, double);
	Cart& operator=(const Cart&);
	Cart& operator=(Cart&&);
	double operator()(void) const;
	void operator--(int); //delete all items where quantity=0
	void minusOperatorHelp(int);
	int getNumOfElements() const;
};

//Code by: Aleksa M. 