#include "Item.h"

void Item::copy(const Item& other)
{
	name = new char[std::strlen(other.name) + 1];
	unitOfMeasure = new char[std::strlen(other.unitOfMeasure) + 1];
	std::strcpy(name, other.name);
	std::strcpy(unitOfMeasure, other.unitOfMeasure);
	name[std::strlen(name)] = unitOfMeasure[std::strlen(unitOfMeasure)] = '\0';
	unitPrice = other.unitPrice;
}

inline void Item::move(Item&& other)
{
	name = other.name;
	unitOfMeasure = other.unitOfMeasure;
	other.name = other.unitOfMeasure = nullptr;
	unitPrice = other.unitPrice;
}

Item::Item(double unitPrice) : unitPrice(unitPrice)
{
	name = new char[2];
	unitOfMeasure = new char[2];
	std::strcpy(name, "?");
	std::strcpy(unitOfMeasure, "?");
	name[1] = unitOfMeasure[1] = '\0';
}

Item::Item(const Item& other) { copy(other); }

Item::Item(Item&& other) { move(std::move(other)); }

Item::Item(const char* name, const char* unitOfMeasure, int unitPrice) : unitPrice(unitPrice)
{
	this->name = new char[std::strlen(name)+1];
	this->unitOfMeasure = new char[std::strlen(unitOfMeasure)+1];
	std::strcpy(this->name, name);
	std::strcpy(this->unitOfMeasure, unitOfMeasure);
	this->name[std::strlen(name)] = this->unitOfMeasure[std::strlen(unitOfMeasure)] = '\0';
}

Item::~Item()
{
	delete[] name;
	delete[] unitOfMeasure;
	name = unitOfMeasure = nullptr;
	unitPrice = 0;
}

Item& Item::operator=(const Item& other)
{
	if (this == &other)
		return *this;

	delete[] name;
	delete[] unitOfMeasure;
	copy(other);

	return *this;
}

Item& Item::operator=(Item&& other)
{
	if (this == &other)
		return *this;

	delete[] name;
	delete[] unitOfMeasure;
	move(std::move(other));

	return *this;
}

bool Item::operator==(const Item& other) const { return std::strcmp(name, other.name) == 0 ? 1 : 0; }

bool Item::operator!=(const Item& other) const { return !(*this == other); }

double Item::getUnitPrice() const { return unitPrice; }

std::ostream& operator<<(std::ostream& out, const Item& item) { return out << item.name << ": " << item.unitPrice << " KM x " << item.unitOfMeasure; }

std::istream & operator >> (std::istream& in, Item& item)
{
	char n[30], uOM[10];
	item.~Item();
	in >> n >> uOM >> item.unitPrice;
	item.name = new char[std::strlen(n) + 1];
	item.unitOfMeasure = new char[std::strlen(uOM) + 1];
	std::strcpy(item.name, n);
	std::strcpy(item.unitOfMeasure, uOM);
	item.name[std::strlen(item.name)] = item.unitOfMeasure[std::strlen(item.unitOfMeasure)] = '\0';
	return in;
}

//Code by: Aleksa M. 