#pragma once
#include <iostream>
#include <cstring>

class Item
{
	char* name;
	char* unitOfMeasure;
	double unitPrice;
	void copy(const Item&);
	inline void move(Item&&);
	friend std::ostream& operator<<(std::ostream&, const Item&);
	friend std::istream& operator >> (std::istream&, Item&);
public:
	Item(double = 0);
	Item(const Item&);
	Item(Item&&);
	Item(const char*, const char*, int);
	~Item();
	Item& operator=(const Item&);
	Item& operator=(Item&&);
	bool operator==(const Item&) const;
	bool operator!=(const Item&) const;
	double getUnitPrice() const;
	char*& getName() = delete;
	char*& getUnitOfMeasure() = delete;
};

//Code by: Aleksa M. 