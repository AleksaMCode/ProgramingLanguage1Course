#include "ItemQuantity.h"

ItemQuantity::ItemQuantity(Item art, double quantity) : art(art), quantity(quantity) {}

ItemQuantity::ItemQuantity(const ItemQuantity& other)
{
	art = other.art;
	quantity = other.quantity;
}

bool ItemQuantity::operator+=(const double num) { return (quantity += num) < 0 ? true : false; }

double ItemQuantity::operator()(void) const { return art.getUnitPrice()*quantity; }

Item& ItemQuantity::getArt() { return art; }

double& ItemQuantity::getQuantity() { return quantity; }

std::ostream& operator<<(std::ostream& out, const ItemQuantity& iq) { return out << iq.art << " " << iq.quantity << " (" << iq() << "KM)"; }

//Code by: Aleksa M. 