#pragma once
#include <iostream>
#include "Item.h"

class ItemQuantity
{
	Item art;
	double quantity;
	friend std::ostream& operator<<(std::ostream&, const ItemQuantity&);
public:
	ItemQuantity(Item = 0, double = 0);
	ItemQuantity(const ItemQuantity&);
	bool operator+=(const double); //no const after, because function changes object
	double operator()(void) const;
	Item& getArt();
	double& getQuantity();
};

//Code by: Aleksa M. 