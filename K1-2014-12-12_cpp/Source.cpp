#include "Cart.h"
#include <cctype>

int main()
{
	Cart cartMain;
	char c;
	do
	{
		std::cout << "Add [Y\\N]? "; std::cin >> c;
		if (std::toupper(c) == 'Y')
		{
			Item temp(0); double q;
			std::cout << "Item : "; std::cin >> temp;
			std::cout << "Quantity : "; std::cin >> q;
			cartMain(temp, q);
		}
	} while (std::toupper(c) != 'N');

	cartMain--; 

	std::cout << "==================================================" << std::endl;
	std::cout << "Cart: " << cartMain() << " KM (Total: " << cartMain.getNumOfElements() << ")" << std::endl;
	std::cout << "--------------------------------------------------" << std::endl;
	std::cout << cartMain;
	std::cout << "==================================================" << std::endl;
}

//Code by: Aleksa M. 