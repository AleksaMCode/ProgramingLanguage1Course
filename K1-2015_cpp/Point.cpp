#include "Point.h"

namespace midtermExam
{
	Point::Point(char* name, double x, double y) : name(name), x(x), y(y) {}

	Point::Point(double x, double y) : name(""), x(x), y(y) {}

	Point& Point::operator=(const Point& other)
	{
		if (this == &other) return *this;

		name = other.name;
		x = other.x;
		y = other.y;

		return *this;
	}

	bool Point::operator==(const Point& other) const { return x == other.x && y == other.y; }

	bool Point::operator!=(const Point& other) const { return !(*this == other); }

	double Point::operator-(const Point& other) const { return std::sqrt(std::pow(other.x - x, 2) + std::pow(other.y - y, 2)); }

	Point::operator double() { return std::sqrt(x*x + y*y); }

	inline void Point::setXY(double x, double y) { this->x = x; this->y = y; }

	inline double Point::getX() const { return x; }

	inline double Point::getY() const { return y; }

	void Point::read() { std::cin >> name >> x >> y; }

	void Point::print() const { std::cout << name << "(" << x << ", " << y << ")" << std::endl; }

	std::istream& operator >> (std::istream& in, Point& p) { return in >> p.name >> p.x >> p.y; }

	/*-------------------------------------------*/
	void ArrayP::copy(const ArrayP& other)
	{
		array = new Point[size = other.size];
		std::copy(other.array, other.array + size, array);
	}

	inline void ArrayP::move(ArrayP& other)
	{
		array = other.array;
		size = other.size;
		other.array = nullptr;
	}

	void ArrayP::apply(double num, std::function<double(double, double)> f) const
	{
		for (int i = 0; i < size; ++i)
			array[i].setXY(f(array[i].getX(), num), f(array[i].getY(), num));
	}

	ArrayP::ArrayP(int size) : size(size) { array = new Point[this->size]; }

	ArrayP::ArrayP(const ArrayP& other) { copy(other); }

	ArrayP::ArrayP(ArrayP&& other) { move(std::move(other)); }

	ArrayP::~ArrayP()
	{
		delete[] array;
		size = 0;
	}

	ArrayP ArrayP::filter(const std::function<bool(const Point&)>&f) const
	{
		int count = 0;
		for (int i = 0; i < size; ++i)
			if (f(array[i]) == true) count++;

		if (!count) return ArrayP(1);

		ArrayP temp(count);
		for (int i = 0, j = 0; i < size; ++i)
			if (f(array[i]) == true)
				temp.array[j++] = array[i];

		return temp;
	}

	ArrayP& ArrayP::operator=(const ArrayP& other)
	{
		if (this != &other) {

			delete[] array;
			copy(other);
		}
		return *this;
	}

	ArrayP& ArrayP::operator=(ArrayP&& other)
	{
		if (this == &other)
			return *this;

		delete[] array;
		move(std::move(other));
		return *this;
	}

	void ArrayP::operator+=(double num) const
	{
		if (!num) return;
		apply(num, [](double a, double b) {return a + b; });
	}

	void ArrayP::operator-=(double num) const
	{
		if (!num) return;
		apply(num, [](double a, double b) {return a - b; });
	}

	void ArrayP::operator*=(double num) const { apply(num, [](double a, double b) {return a * b; }); }

	Point& ArrayP::operator[](int n) const
	{
		if (n < 0 || n > size)
			return array[0];

		else return array[n];
	}

	std::ostream& operator<<(std::ostream& out, const ArrayP& arr)
	{
		out << "[(" << arr.array[0].getX() << ", " << arr.array[0].getY() << ")";
		std::for_each(arr.array + 1, arr.array + arr.size, [&out](Point& el) {out << ", (" << el.getX() << ", " << el.getY() << ")"; });
		return out << "]";
	}

	std::iostream& operator >> (std::iostream& in, ArrayP& arr)
	{
		std::for_each(arr.array, arr.array + arr.size, [&in](Point& el) { std::cout << ">> (x,y) "; in >> el; });
		return in;
	}

}

//Code by: Aleksa M. 