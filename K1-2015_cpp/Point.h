#pragma once
#include <iostream>
#include <functional>
#include <algorithm>
#include <string>
#include <cmath>

namespace midtermExam
{
	class Point
	{
		double x, y;
		std::string name;
		friend std::istream& operator >> (std::istream&, Point&);
	public:
		Point(char*, double, double);
		Point(double = 0, double = 0);
		Point& operator=(const Point&);
		bool operator==(const Point&) const;
		bool operator!=(const Point&) const;
		double operator-(const Point&) const;
		operator double();
		inline void setXY(double, double);
		inline double getX() const;
		inline double getY() const;
		void read();
		void print() const;
	};

	const Point theOrigin;

	class ArrayP
	{
		Point *array;
		int size;
		void copy(const ArrayP&);
		inline void move(ArrayP&);
		void apply(double, std::function<double(double, double)>) const;
		friend std::ostream& operator<<(std::ostream&, const ArrayP&);
		friend std::iostream& operator >> (std::iostream&, ArrayP&);
	public:
		ArrayP(int = 1);
		ArrayP(const ArrayP&); //copy const.
		ArrayP(ArrayP&&); //move constr.
		~ArrayP();
		ArrayP filter(const std::function<bool(const Point&)>&) const;
		ArrayP& operator=(const ArrayP&);
		ArrayP& operator=(ArrayP&&);
		void operator+=(double) const;
		void operator-=(double) const;
		void operator*=(double) const;
		Point& operator[](int) const;
	};
}

//Code by: Aleksa M. 