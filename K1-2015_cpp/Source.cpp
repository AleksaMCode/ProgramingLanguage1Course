#include "Point.h"
using namespace midtermExam;

int main()
{
	/*midtermExam::*/Point a, b("B", 2, 4), c("C", 1, 2), d;
	std::cout << "Enter "; 	d.read();
	a.print();
	b.print();
	c.print();
	d.print();
	//std::cout << (double)b << std::endl;
	//std::cout << (c == d);
	ArrayP arrayMain(5);
	arrayMain[0] = a;
	arrayMain[1] = b;
	arrayMain[2] = c;
	arrayMain[3] = d;
	//std::cin >> arrayMain[4];
	//arrayMain += .1;
	std::cout << arrayMain << std::endl;
	std::cout << arrayMain.filter([&arrayMain](const Point& p) { return p == arrayMain[3]; });
}

//Code by: Aleksa M. 