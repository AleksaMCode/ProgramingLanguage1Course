#include "BufferdList.h"
using namespace test;

namespace test
{
	Point::Point(double x, double y) : x(x), y(y) {}

	std::ostream& operator<<(std::ostream& out, const Point& p) { return out << "(" << p.x << "," << p.y << ")" << std::endl; }

	/*--------------------------------------------------------------*/

	void BufferedList::copy(const BufferedList& other)
	{
		array = new Point[currentCapacity = numOfElements = other.numOfElements];
		std::copy(other.array, other.array + numOfElements, array);

		changeConstValue(other.listCapacity); //No longer needed
	}

	inline void BufferedList::move(BufferedList&& other)
	{
		array = other.array;
		other.array = nullptr;
		currentCapacity = other.currentCapacity;
		numOfElements = other.numOfElements;

		changeConstValue(other.listCapacity); //No longer needed
	}

	BufferedList::BufferedList(uInt listCapacity) : listCapacity(listCapacity)
	{
		array = new Point[currentCapacity = listCapacity];
		std::fill(array, array + currentCapacity, 0);
	}

	BufferedList::BufferedList(const BufferedList& other) : listCapacity(other.listCapacity) { copy(other); }

	BufferedList::BufferedList(BufferedList&& other) : listCapacity(other.listCapacity) { move(std::move(other)); }

	BufferedList::~BufferedList() { delete[] array; array = nullptr; currentCapacity = numOfElements = 0; }

	BufferedList& BufferedList::operator=(const BufferedList& other)
	{
		if (this == &other)
			return *this;

		delete[] array;
		copy(other);
		return *this;
	}

	BufferedList& BufferedList::operator=(BufferedList&& other)
	{
		if (this != &other)
		{
			delete[] array;
			move(std::move(other));
		}
		return *this;
	}

	void BufferedList::add(const Point& other)
	{
		if (currentCapacity == numOfElements + 1) realloc();
		array[numOfElements++] = other;
	}

	void BufferedList::realloc()
	{
		Point *tmp = new Point[currentCapacity + listCapacity];
		std::copy(array, array + numOfElements, tmp);
		delete[] array;
		array = new Point[currentCapacity = currentCapacity + listCapacity];
		array = tmp;
		tmp = nullptr;
	}

	Point& BufferedList::operator[](uInt index)
	{
		if (index < 0 || index > numOfElements)
			return array[0];
		return array[index];
	}

	const Point& BufferedList::operator[](uInt index) const { return const_cast<Point &>(const_cast<BufferedList&>(*this)[index]); }

	BufferedList BufferedList::operator()(std::function<bool(const Point&)>f) const
	{
		BufferedList tmp;
		for (uInt i = 0; i < numOfElements; ++i)
			if (f(array[i])) tmp.add(array[i]);

		return tmp; //MV
	}

	void BufferedList::changeConstValue(uInt listCapacity)
	{
		uInt *p = const_cast<uInt*>(&this->listCapacity);
		*p = listCapacity;
	}

	std::ostream &operator<<(std::ostream &out, const BufferedList& list)
	{
		std::for_each(list.array, list.array + list.numOfElements, [&out](Point& el) {out << el; });
		return out;
	}
}

//Code by: Aleksa M. 