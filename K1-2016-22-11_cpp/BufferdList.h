#pragma once
#include <iostream>
#include <algorithm>
#include <functional>

using cui = const unsigned int;
using uInt = unsigned int;

namespace test
{
	class Point
	{
		friend std::ostream& operator<<(std::ostream&, const Point&);
	public:
		double x, y;
		Point(double = 0, double = 0);
	};
	/*--------------------------------------------------------------*/
	cui n = 16;
	class BufferedList
	{
		Point *array;
		cui listCapacity = n;
		uInt currentCapacity = 0, numOfElements = 0;
		void copy(const BufferedList&);
		inline void move(BufferedList&&);
		friend std::ostream& operator<<(std::ostream&, const BufferedList&);
	public:
		explicit BufferedList(uInt = n);
		BufferedList(const BufferedList&);
		BufferedList(BufferedList&&);
		~BufferedList();
		BufferedList& operator=(const BufferedList&);
		BufferedList& operator=(BufferedList&&);
		void add(const Point&);
		void realloc();
		Point& operator[](uInt);
		const Point& operator[](uInt) const;
		BufferedList operator()(std::function<bool(const Point&)>) const;
		void changeConstValue(uInt);
	};
}

//Code by: Aleksa M. 