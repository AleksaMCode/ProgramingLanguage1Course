#include "BufferdList.h"
using namespace test;
//1. midterm exam - 22.11.2016.

int main()
{
	BufferedList list;
	Point* mainArray = new Point[10];

	for (uInt i(0); i < 10; ++i)
	{
		mainArray[i].x = mainArray[i].y = i;
		list.add(mainArray[i]);
	}

	delete[] mainArray;
	Point pArray[10];
	for (uInt i = 0; i < 4; ++i)
		pArray[i].x = 1, pArray[i].y = i + 2;

	for (const Point& el : pArray) list.add(el); //MV
	std::cout << list([](const Point& el) {return el.x > 0; });
}

//Code by: Aleksa M. 