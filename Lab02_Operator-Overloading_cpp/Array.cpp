#include "Array.h"

void Array::copy(const Array& other)
{
	array = new int[size = other.size];
	std::copy(other.array, other.array + size, array);
}

inline void Array::move(Array&& other)
{
	array = other.array;
	size = other.size;
	other.array = nullptr;
}

Array Array::apply(const Array& other, std::function<int(int, int)> f) const
{
	if (size != other.size) return Array(1);
	Array temp(size);
	for (int i = 0; i < size; ++i)
		temp.array[i] = f(array[i], other.array[i]);
	return temp;
}

Array::Array(int size, int fill)
{
	array = new int[this->size = size];
	std::fill(array, array + size, fill);
}

Array::Array(const Array& other) { copy(other); }

Array::Array(Array&& other) { move(std::move(other)); }

Array::~Array() { delete[] array; size = 0; }

Array Array::filter(std::function<bool(const int)> f) const
{
	int count = 0;
	for (int i = 0; i < size; ++i)
		if (f(array[i])) count++;

	if (!count) return Array(1);

	Array temp(count);
	for (int i = 0, j = 0; i < size; ++i)
		if (f(array[i])) temp.array[j++] = array[i];
	return temp;
}

Array& Array::operator=(const Array& other)
{
	if (this == &other)
		return *this;

	delete[] array; //delete existing array

	copy(other);
	return *this;
}

Array& Array::operator=(Array&& other)
{
	if (this == &other)
		return *this;

	delete[] array; //delete existing array

	move(std::move(other));
	return *this;
}

Array Array::operator-(const Array& other) const { return apply(other, [](int a, int b) {return a - b; }); }

Array Array::operator+(const Array& other) const { return apply(other, [](int a, int b) {return a + b; }); }

Array Array::operator/(const Array& other) const { return apply(other, [](int a, int b) {return a / b; }); }

Array Array::operator*(const Array& other) const { return apply(other, [](int a, int b) {return a * b; }); }

int Array::operator[](int i) const
{
	if (i < 0 || i >= size)
		return 0;
	return array[i];
}
double Array::operator()() const
{
	double art = 0;
	std::for_each(array, array + size, [&art](int el) {art += el; });
	return art /= size;
}
bool Array::operator==(const Array& other) const
{
	if (size != other.size) return false;
	for (int i = 0; i < size; ++i)
		if (array[i] != other.array[i]) return false;
	return true;
}

bool Array::operator!=(const Array& arr) const { return !(*this == arr); }

std::ostream & operator<<(std::ostream& out, const Array& arr)
{
	out << "(" << arr.array[0];
	std::for_each(arr.array + 1, arr.array + arr.size, [&out](int& el) {out << ", " << el; });
	return out << ")";
}

std::istream & operator>>(std::istream& in, Array& arr)
{
	std::for_each(arr.array, arr.array + arr.size, [&in](int& el) { std::cout << ">> "; in >> el; });
	return in;
}

//Code by: Aleksa M. 