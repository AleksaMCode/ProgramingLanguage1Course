#pragma once
#include <iostream>
#include <functional>
#include <algorithm>

class Array
{
	int *array;
	int size;
	void copy(const Array&);
	inline void move(Array&&);
	Array apply(const Array&, std::function<int(int, int)>) const;
	friend std::ostream& operator<<(std::ostream&, const Array&);
	friend std::istream& operator>>(std::istream&, Array&);
public:
	Array(int = 5, int = 0);
	Array(const Array&);
	Array(Array&&);
	~Array(); 
	Array filter(std::function<bool(const int)>) const;
	Array& operator=(const Array&);
	Array& operator=(Array&&);
	Array operator-(const Array&) const;
	Array operator+(const Array&) const;
	Array operator*(const Array&) const;
	Array operator/(const Array&) const;
	int operator[](int) const;
	double operator()() const; //modification
	bool operator==(const Array&) const;
	bool operator!=(const Array&) const;

	void read() = delete;
	void print() const = delete;

};

//Code by: Aleksa M. 