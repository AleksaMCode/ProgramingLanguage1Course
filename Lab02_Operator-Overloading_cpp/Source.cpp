#include <iostream>
#include "Array.h"

using std::cin;

int main()
{
	Array a, b;
	std::cout << "Insert first: " << std::endl;
	cin >> a;
	//std::cout << "Insert second: " << std::endl;
	//cin >> b;
		
	std::cout << "Average for first array: " << a() << std::endl;
	std::cout << "Filtered:" << std::endl;
	std::cout << a.filter([](const int num) {return num > 0; }); //filter greater than 0
}

//Code by: Aleksa M. 