#include "ArrayOfInt.h"

ArrayOfInt::ArrayOfInt(int n)
{
	array = new int[this->n = n];
	std::fill(array, array + n, 0);
}

ArrayOfInt::ArrayOfInt(const ArrayOfInt& other)
{
	array = new int[n = other.n];
	std::copy(other.array, other.array + n, array);
}

ArrayOfInt::ArrayOfInt(ArrayOfInt&& other)
{
	array = other.array;
	n = other.n;
	other.array = nullptr;
}

ArrayOfInt::~ArrayOfInt() { delete[] array; n = 0; }

void add(int num, int pos, ArrayOfInt& arr)
{
	if (arr.n == arr.fill || arr.array[--pos] != 0) return; //array is full or position is occupied
	else arr.array[pos] = num, ++arr.fill;
}

void remove(int pos, ArrayOfInt& arr)
{
	if (arr.array[pos] == 0) return;
	else arr.array[pos] = 0, --arr.fill;
}

bool compare(const ArrayOfInt& first, const ArrayOfInt& second)
{
	if (first.n != second.n) return false;
	for (int i = 0; i < first.n; ++i)
		if (first.array[i] != second.array[i]) return false;
	return true;
}

void print(const ArrayOfInt& arr)
{
	std::cout << "( " << arr.array[0];
	std::for_each(arr.array + 1, arr.array + arr.n, [](const int& el) {std::cout << ", " << el; });
	std::cout << " )" << std::endl;
}

//Code by: Aleksa M. 