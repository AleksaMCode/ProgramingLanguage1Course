#pragma once
#include <iostream>
#include <algorithm>

class ArrayOfInt
{
	int *array;
	int n;
	int fill = 0;
public:
	ArrayOfInt(int = 5);
	ArrayOfInt(const ArrayOfInt&);
	ArrayOfInt(ArrayOfInt&&);
	~ArrayOfInt();
	friend void add(int, int, ArrayOfInt&);
	friend void remove(int, ArrayOfInt&);;
	friend bool compare(const ArrayOfInt&, const ArrayOfInt&);
	friend void print(const ArrayOfInt&);
};

//Code by: Aleksa M. 