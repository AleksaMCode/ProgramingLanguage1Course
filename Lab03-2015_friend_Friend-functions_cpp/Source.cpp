#include <iostream>
#include "ArrayOfInt.h"

int main()
{
	ArrayOfInt a1, a2;
	add(4, 1, a1); add(3, 2, a1); add(8, 3, a1); add(56, 4, a1); add(22, 5, a1);
	add(4, 1, a2); add(3, 2, a2); add(8, 3, a2); add(56, 4, a2); add(22, 5, a2);
	print(a1); print(a2);
	std::cout << compare(a1, a2);
	remove(2, a1);
	std::cout << " " << compare(a1, a2) << std::endl;
}

//Code by: Aleksa M. 