#include "Collection_.h"

Collection::Collection(int cap) : currentCapacity(cap > 0 ? cap : listCapacity)
{
	array = new Rectangle[cap];
	std::fill(array, array + currentCapacity, 0);
}

Collection::Collection(const Collection& other) { copy(other); }

Collection::Collection(Collection&& other) { move(std::move(other)); }

Collection::~Collection()
{
	delete[] array;
	array = nullptr;
	currentCapacity = numOfElements = 0;
}

Collection& Collection::operator=(const Collection& other)
{
	if (this != &other)
	{
		delete[] array;
		copy(other);
	}
	return *this;
}

Collection& Collection::operator=(Collection&& other)
{
	if (this != &other)
	{
		delete[] array;
		move(std::move(other));
	}
	return *this;
}

void Collection::add(const Rectangle& num)
{
	if (currentCapacity == numOfElements + 1) realloc();
	array[numOfElements++] = num;
}

void Collection::realloc()
{
	Rectangle *tmp = new Rectangle[currentCapacity + listCapacity];
	std::copy(array, array + numOfElements, tmp);
	delete[] array;
	array = new Rectangle[currentCapacity = currentCapacity + listCapacity];
	std::copy(tmp, tmp + numOfElements, array);
	delete[] tmp;
}

void Collection::fitRealloc()
{
	if (numOfElements == currentCapacity) return;
	Rectangle *tmp = new Rectangle[currentCapacity = numOfElements];
	std::copy(array, array + numOfElements, tmp);
	delete[] array;
	array = new Rectangle[numOfElements];
	std::copy(tmp, tmp + numOfElements, array);
	delete[] tmp;
}

void Collection::copy(const Collection& other)
{
	array = new Rectangle[currentCapacity = numOfElements = other.currentCapacity];
	std::copy(other.array, other.array + numOfElements, array);
}

inline void Collection::move(Collection&& other)
{
	array = other.array;
	other.array = nullptr;
	currentCapacity = other.currentCapacity;
	numOfElements = numOfElements;
}

Rectangle& Collection::operator[](int index)
{
	if (index < 0 || index > numOfElements)
		exit(1);
	return array[index];
}

const Rectangle& Collection::operator[](int index) const { return const_cast<Rectangle &>(const_cast<Collection&>(*this)[index]); }


void Collection::print(std::ostream& str) const
{
	str << "{" << array[0].getA() << "x" << array[0].getB();
	std::for_each(array + 1, array + numOfElements, [&str](const Rectangle& el) { str << ", " << el.getA() << "x" << el.getB(); });
	str << "}";
}

void Collection::read(std::istream& str) { std::for_each(array, array + numOfElements, [&str](Rectangle& el) { str >> el.changeA() >> el.changeB(); }); }

std::ostream& operator<<(std::ostream& out, const Collection& col)
{
	col.print(out);
	return out;
}

std::istream & operator >> (std::istream& in, Collection& col)
{
	col.read(in);
	return in;
}

//Code by: Aleksa M. 