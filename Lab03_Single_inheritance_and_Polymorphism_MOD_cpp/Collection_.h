#pragma once
#include <iostream>
#include <algorithm>
#include <functional>
#include "Rectangle_.h"

using ci = const int;

class Collection
{
protected:
	Rectangle *array;
	ci listCapacity = 10;
	int currentCapacity = 0, numOfElements = 0;
	virtual void print(std::ostream&) const;
	virtual void read(std::istream&);

private:
	void copy(const Collection&);
	inline void move(Collection&&);
	friend std::ostream& operator<<(std::ostream&, const Collection&);
	friend std::istream& operator >> (std::istream&, Collection&);

public:
	Collection(int = 10);
	Collection(int, std::initializer_list<int> list) = delete;
	Collection(const Collection&);
	Collection(Collection&&);
	virtual ~Collection();
	Collection& operator=(const Collection&);
	Collection& operator=(Collection&&);
	virtual void add(const Rectangle&);
	void realloc();
	void fitRealloc();
	Rectangle& operator[](int);
	const Rectangle& operator[](int) const;
};

//Code by: Aleksa M. 