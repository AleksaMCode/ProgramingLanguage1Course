#include "Rectangle_.h"

Rectangle::Rectangle(int a, int b) : a(a), b(b) {}

int Rectangle::getA() const { return a; }

int Rectangle::getB() const { return b; }

void Rectangle::setA(int a) { this->a = a; }

void Rectangle::setB(int b) { this->b = b; }

int& Rectangle::changeA() { return a; }

int& Rectangle::changeB() { return b; }

//Code by: Aleksa M. 