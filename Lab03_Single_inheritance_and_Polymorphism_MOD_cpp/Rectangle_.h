#pragma once

class Rectangle
{
	int a, b;
public:
	Rectangle(int = 0, int = 0);
	int getA() const;
	int getB() const;
	void setA(int);
	void setB(int);
	int& changeA();
	int& changeB();
};

//Code by: Aleksa M. 