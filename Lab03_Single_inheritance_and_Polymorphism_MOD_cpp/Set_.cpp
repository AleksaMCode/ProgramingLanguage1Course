#include "Set_.h"

Set::Set(int cap) : Collection(cap) {}

void Set::add(const Rectangle& num)
{
	if (checkForDouble(num))
	{
		if (currentCapacity == numOfElements + 1) realloc();
		array[numOfElements++] = num;
	}
}

bool Set::checkForDouble(const Rectangle& el) const
{
	for (int i = int(0); i < numOfElements; ++i)
	{
		if (array[i].getA() == el.getA() && array[i].getB() == el.getB()
			|| array[i].getA() == el.getB() && array[i].getB() == el.getA()) return false;
	}
	return true;
}

//Code by: Aleksa M. 