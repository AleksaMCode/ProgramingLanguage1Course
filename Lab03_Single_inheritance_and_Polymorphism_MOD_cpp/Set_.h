#pragma once
#include "Collection_.h"

class Set : public Collection
{
public:
	Set(int = 10);
	virtual void add(const Rectangle&) override;
	bool checkForDouble(const Rectangle&) const;
};

//Code by: Aleksa M. 