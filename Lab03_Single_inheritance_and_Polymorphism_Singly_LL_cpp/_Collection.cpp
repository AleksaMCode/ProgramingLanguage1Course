#include "_Collection.h"

Collection_::NODE::NODE(const Rectangle& data) : next(nullptr), info(data) {}

void Collection_::print(std::ostream& str) const
{
	str << "(" << head->info.getA() << "x" << head->info.getB();
	for (Collection_::NODE *tmp = head->next; tmp; tmp = tmp->next)
		str << ", " << tmp->info.getA() << "x" << tmp->info.getB();
	str << ")";
}

void Collection_::copy(const Collection_& other)
{
	Collection_::~Collection_();
	for (NODE* node = other.head; node; node->next)
		addHead(node->info);
}

inline void Collection_::move(Collection_&& other)
{
	head = other.head;
	other.head = nullptr;
	size = other.size;
}

Collection_::Collection_() : head(nullptr), size(0) {}

Collection_::Collection_(const Collection_& other) { copy(other); }

Collection_::Collection_(Collection_&& other) { move(std::move(other)); }

Collection_::~Collection_()
{
	while (head)
	{
		NODE *tmp = head->next;
		delete head;
		head = tmp;
	}
	head = nullptr;
	size = 0;
}

Collection_ & Collection_::operator=(const Collection_& other)
{
	if (this != &other)
	{
		this->~Collection_();
		copy(other);
	}
	return *this;
}

Collection_ & Collection_::operator=(Collection_&& other)
{
	if (this != &other)
	{
		this->~Collection_();
		move(std::move(other));
	}
	return *this;
}

void Collection_::addHead(const Rectangle& data)
{
	NODE *new_ = new NODE(data);
	new_->next = head;
	head = new_;
	size++;
}

Rectangle& Collection_::operator[](int index)
{
	if (!isIndexInRange(index))
		exit(1);
	return getElementAt(index)->info;
}

inline bool Collection_::isIndexInRange(int index) const { return index >= 0 && index < size; }

Collection_::NODE* Collection_::getElementAt(int index) const
{
	NODE *tmp;
	for (tmp = head; index; tmp = tmp->next, --index);
	return tmp;
}

std::ostream & operator<<(std::ostream& out, const Collection_& col)
{
	col.print(out);
	return out;
}

//Code by: Aleksa M. 