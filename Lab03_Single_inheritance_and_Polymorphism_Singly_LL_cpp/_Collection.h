#pragma once
#include <iostream>
#include <algorithm>
#include <functional>
#include "_Rectangle.h"

using ci = const int;

class Collection_
{

protected:
	struct NODE
	{
		Rectangle info;
		NODE *next;
		NODE(const Rectangle&);
	};
	int size;
	NODE *head;
	virtual void print(std::ostream&) const;

private:
	void copy(const Collection_&);
	inline void move(Collection_&&);
	friend std::ostream& operator<<(std::ostream&, const Collection_&);

public:
	Collection_();
	Collection_(const Collection_&);
	Collection_(Collection_&&);
	virtual ~Collection_();
	Collection_& operator=(const Collection_&);
	Collection_& operator=(Collection_&&);
	virtual void addHead(const Rectangle&);
	Rectangle& operator[](int);
	inline bool isIndexInRange(int) const;
	NODE* getElementAt(int) const;
};

//Code by: Aleksa M. 