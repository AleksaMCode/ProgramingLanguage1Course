#include "_Rectangle.h"

Rectangle::Rectangle(int a, int b) : a(a), b(b) {}

int Rectangle::getA() const { return a; }

int Rectangle::getB() const { return b; }

void Rectangle::setA(int a) { this->a = a; }

void Rectangle::setB(int b) { this->b = b; }

int& Rectangle::changeA() { return a; }

int& Rectangle::changeB() { return b; }


bool Rectangle::operator==(const Rectangle& other) const
{
	if (this->a == other.a && this->b == other.b || this->a == other.b && this->b == other.a) return true;
	return false;
}

bool Rectangle::operator!=(const Rectangle& other) const { return !(*this == other); }

//Code by: Aleksa M. 