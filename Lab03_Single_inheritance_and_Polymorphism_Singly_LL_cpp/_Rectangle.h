#pragma once

class Rectangle
{
	int a, b;
public:
	Rectangle(int = 1, int = 1);
	int getA() const;
	int getB() const;
	void setA(int);
	void setB(int);
	int& changeA();
	int& changeB();
	bool operator==(const Rectangle&) const;
	bool operator!=(const Rectangle&) const;
};

//Code by: Aleksa M. 