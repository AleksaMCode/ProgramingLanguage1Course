#include "_Set.h"

Set_::Set_() : Collection_() {}

void Set_::addHead(const Rectangle& data)
{
	if (checkForDouble(data))
		Collection_::addHead(data);
}

bool Set_::checkForDouble(const Rectangle& data) const
{
	NODE *tmp;
	for (tmp = head; tmp; tmp = tmp->next)
		if (tmp->info == data) return false;
	return true;
}

void Set_::print(std::ostream& str) const
{
	str << "{" << head->info.getA() << "x" << head->info.getB();
	for (Collection_::NODE *tmp = head->next; tmp; tmp = tmp->next)
		str << ", " << tmp->info.getA() << "x" << tmp->info.getB();
	str << "}";
}

//Code by: Aleksa M. 