#pragma once
#include "_Collection.h"

class Set_ : public Collection_
{
public:
	Set_();
	virtual void addHead(const Rectangle&) override;
	bool checkForDouble(const Rectangle&) const;
	using Collection_::operator=;
protected:
	virtual void print(std::ostream&) const override;
};

//Code by: Aleksa M. 