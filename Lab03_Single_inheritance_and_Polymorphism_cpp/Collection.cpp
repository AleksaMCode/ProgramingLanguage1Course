#include "Collection.h"

Collection::Collection(int cap) : currentCapacity(cap > 0 ? cap : listCapacity)
{
	array = new int[cap];
	std::fill(array, array + currentCapacity, 0);
}

Collection::Collection(int cap, std::initializer_list<int> list) : currentCapacity(cap > 0 ? cap : listCapacity), numOfElements(list.size())
{
	if (list.size() > static_cast<unsigned>(cap)) exit(2);	
	array = new int[cap];
	std::copy(list.begin(), list.end(), array);
}

Collection::Collection(const Collection& other) { copy(other); }

Collection::Collection(Collection&& other) { move(std::move(other)); }

Collection::~Collection()
{
	delete[] array;
	array = nullptr;
	currentCapacity = numOfElements = 0;
}

Collection& Collection::operator=(const Collection& other)
{
	if (this != &other)
	{
		delete[] array;
		copy(other);
	}
	return *this;
}

Collection& Collection::operator=(Collection&& other)
{
	if (this != &other)
	{
		delete[] array;
		move(std::move(other));
	}
	return *this;
}

void Collection::add(const int num)
{
	if (currentCapacity == numOfElements + 1) realloc();
	array[numOfElements++] = num;
}

void Collection::realloc()
{
	int *tmp = new int[currentCapacity + listCapacity];
	std::copy(array, array + numOfElements, tmp);
	delete[] array;
	array = new int[currentCapacity = currentCapacity + listCapacity];
	/*
	std::copy(tmp, tmp + numOfElements, array);
	delete[] tmp;
	OR           */
	array = tmp;
	tmp = nullptr;
}

void Collection::fitRealloc()
{
	if (numOfElements == currentCapacity) return;
	int *tmp = new int[currentCapacity = numOfElements];
	std::copy(array, array + numOfElements, tmp);
	delete[] array;
	array = new int[numOfElements];
	/*
	std::copy(tmp, tmp + numOfElements, array);
	delete[] tmp;
	OR           */
	array = tmp;
	tmp = nullptr;
}

void Collection::copy(const Collection& other)
{
	array = new int[currentCapacity = numOfElements = other.currentCapacity];
	std::copy(other.array, other.array + numOfElements, array);
}

inline void Collection::move(Collection&& other)
{
	array = other.array;
	other.array = nullptr;
	currentCapacity = other.currentCapacity;
	numOfElements = numOfElements;
}

int& Collection::operator[](int index)
{
	if (index < 0 || index > numOfElements)
		exit(1);
	return array[index];

}

const int& Collection::operator[](int index) const { return const_cast<int &>(const_cast<Collection&>(*this)[index]); }


void Collection::print(std::ostream& str) const
{
	str << "(" << array[0];
	std::for_each(array + 1, array + numOfElements, [&str](const int& el) { str << ", " << el; });
	str << ")";
}

void Collection::read(std::istream& str) { std::for_each(array, array + numOfElements, [&str](int& el) { str >> el; }); }

std::ostream& operator<<(std::ostream& out, const Collection& col)
{
	col.print(out);
	return out;
}

std::istream & operator >> (std::istream& in, Collection& col)
{
	col.read(in);
	return in;
}

//Code by: Aleksa M. 