#pragma once
#include <iostream>
#include <algorithm>
#include <functional>

using ci = const int;

class Collection
{

protected:
	int *array;
	ci listCapacity = 10;
	int currentCapacity = 0, numOfElements = 0;
	virtual void print(std::ostream&) const;
	virtual void read(std::istream&);

private:
	void copy(const Collection&);
	inline void move(Collection&&);
	friend std::ostream& operator<<(std::ostream&, const Collection&);
	friend std::istream& operator >> (std::istream&, Collection&);

public:
	Collection(int = 10);
	Collection(int, std::initializer_list<int> list);
	Collection(const Collection&);
	Collection(Collection&&);
	virtual ~Collection();
	Collection& operator=(const Collection&);
	Collection& operator=(Collection&&);
	virtual void add(const int);
	void realloc();
	void fitRealloc();
	int& operator[](int);
	const int& operator[](int) const;
};

//Code by: Aleksa M. 