#include "Set.h"

Set::Set(int cap) : Collection(cap) {}

void Set::add(const int num)
{
	if (checkForDouble(num))
	{
		if (currentCapacity == numOfElements + 1) realloc();
		array[numOfElements++] = num;
	}
}

bool Set::checkForDouble(const int el) const
{
	for (int i = int(0); i < numOfElements; ++i)
		if (array[i] == el) return false;
	return true;
}

void Set::print(std::ostream& str) const
{
	Collection::print(str);
	str << " -> (" << numOfElements << ")" << std::endl;
}

//Code by: Aleksa M. 