#pragma once
#include "Collection.h"

class Set : public Collection
{
public:
	Set(int = 10);
	virtual void add(const int) override;
	bool checkForDouble(const int) const;
	virtual void print(std::ostream&) const override;
};

//Code by: Aleksa M. 