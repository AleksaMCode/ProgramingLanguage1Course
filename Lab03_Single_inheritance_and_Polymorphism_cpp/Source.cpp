#include "Collection.h"
#include "Set.h"

int main()
{
	Collection c(15, { 1,2,3,4,5,6,7,8,9,10,1,2,3,4,5 }); //watch out, the list should not have more elements of a given capacity
	Set s;
	int array[20] = { 1, 1, 1 };
	for (int i(3); i < 20; ++i)
		array[i] = i;
	for (const int& el : array) s.add(el);
	s.fitRealloc();
	//s[0] = 3; //possible duplication of elements when changing the existing value
	//c = s;
	std::cout << c << std::endl;
	std::cout << s;
}

//Code by: Aleksa M. 