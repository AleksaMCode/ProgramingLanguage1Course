#include <iostream>
#include "Text.h"

int main()
{
	Text t1(static_cast<std::string>("This is ")), t2(static_cast<std::string>("text"));
	Text comb = t1 + t2;
	t1 += t2;
	std::cout << comb << std::endl << t1 << std::endl;
	Text comb2	 = t1 + t2;
	std::cout << comb2 << std::endl;
	Text t3(5);
	std::cout << "Insert Text (size = 5):" << std::endl << ">> ";
	std::cin >> t3;
	std::cout << t3 << std::endl;
}

//Code by: Aleksa M. 