#include "Text.h"

void Text::copy(const Text& other)
{
	array = new char[size = other.size];
	std::copy(other.array, other.array + size, array);
}

inline void Text::move(Text&& other)
{
	array = other.array;
	size = other.size;
	other.array = nullptr;
}

Text::Text(int size) { array = new char[this->size = size]; }

Text::Text(std::string& other)
{
	array = new char[size = other.size()];
	for (int i = 0; i < size; ++i)
		array[i] = other[i];
}

Text::Text(const Text& other) { copy(other); }

Text::Text(Text&& other) { move(std::move(other)); }

Text::~Text() { delete[] array; size = 0; }

Text & Text::operator=(const Text& other)
{
	if (size != other.size) return *this;
	copy(other);
	return *this;
}

Text & Text::operator=(Text&& other)
{
	if (size != other.size) return *this;
	move(std::move(other));
	return *this;
}

Text Text::operator+(Text& other)
{
	Text tmp(size + other.size);
	int i = 0;

	for (; i < size; ++i)
		tmp.array[i] = array[i];

	for (int j = 0; j < other.size; ++j)
		tmp.array[i++] = other.array[j];

	return tmp;
}

void Text::operator+=(Text& other)
{
	Text temp = *this + other;
	delete[] array;
	copy(temp);
}

std::ostream& operator<<(std::ostream& out, const Text& txt)
{
	std::for_each(txt.array, txt.array + txt.size, [&out](char& el) { out << el; });
	return out;
}

std::istream& operator>>(std::istream& in, Text& txt)
{
	std::for_each(txt.array, txt.array + txt.size, [&in](char& el) { in >> el; });
	return in;
}

//Code by: Aleksa M. 