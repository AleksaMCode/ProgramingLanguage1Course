#pragma once
#include <iostream>
#include <algorithm>
#include <functional>
#include <string>

class Text
{
	char *array;
	int size;
	void copy(const Text&);
	inline void move(Text&&);
	friend std::ostream& operator<<(std::ostream&, const Text&);
	friend std::istream& operator>>(std::istream&, Text&);
public:
	Text(int = 5);
	Text(std::string&);
	Text(const Text&);
	Text(Text&&);
	~Text();
	Text& operator=(const Text&);
	Text& operator=(Text&&);
	Text operator+(Text&); //concatenation
	void operator+=(Text&); //add at the end
};

//Code by: Aleksa M. 