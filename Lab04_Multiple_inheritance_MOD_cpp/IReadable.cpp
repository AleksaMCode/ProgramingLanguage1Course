#include "IReadable.h"

std::istream & operator >> (std::istream& in, IReadable& ird)
{
	ird.read(in);
	return in;
}

//Code by: Aleksa M.