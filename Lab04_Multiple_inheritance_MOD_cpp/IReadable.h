#pragma once
#include <iostream>

class IReadable
{
	friend std::istream& operator >> (std::istream&, IReadable&);
protected:
	virtual void read(std::istream&) = 0;
};

//Code by: Aleksa M.