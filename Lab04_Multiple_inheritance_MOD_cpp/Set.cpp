#include "Set.h"

Set::Set(int cap) : Collection(cap > 0 ? cap : listCapacity) {}

Set::Set(int cap, std::initializer_list<int> list) : Collection(cap > 0 ? cap : listCapacity, list) {}

void Set::add(const int num)
{
	if (checkForDouble(num))
	{
		if (currentCapacity == numOfElements + 1) realloc();
		array[numOfElements++] = num;
	}
}

bool Set::checkForDouble(const int el) const
{
	for (int i = int(0); i < numOfElements; ++i)
		if (array[i] == el) return false;
	return true;
}

void Set::print(std::ostream& str) const
{
	str << name << " ";
	Collection::print(str);
}

const std::string & Set::getName() const { return name; }

void Set::setName(const std::string& name) { this->name = name; }

//Code by: Aleksa M.