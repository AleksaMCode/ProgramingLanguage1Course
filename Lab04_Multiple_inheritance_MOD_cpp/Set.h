#pragma once
#include "Collection.h"
#include "iNamedElement.h"

class Set : public Collection, virtual public iNamedElement
{
protected:
	std::string name;
public:
	Set(int = 10);
	Set(int, std::initializer_list<int>);
	virtual void add(const int) override;
	bool checkForDouble(const int) const;
	virtual void print(std::ostream&) const override;
	virtual const std::string& getName() const override;
	virtual void setName(const std::string&) override;
};

//Code by: Aleksa M. 