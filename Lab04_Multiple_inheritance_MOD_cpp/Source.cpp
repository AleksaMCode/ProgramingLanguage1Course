#include "TraversableSet.h"

inline void setName(std::string& str) { std::cout << "NAME: "; std::cin >> str; }
inline void setNOfEl(int& n) { std::cout << "Number of elements: "; std::cin >> n; }
void printElements(Collection** arr, int max)
{
	std::cout << std::endl << "==============================" << std::endl;
	for (int j = 0; j < max; ++j)
	{
		std::cout << *arr[j] << std::endl;
		delete[] arr[j];
	}
	std::cout << "==============================" << std::endl;
}

int main()
{
	Collection* array[10];
	char c;
	int k = 0;
	do
	{
		system("PAUSE"), system("CLS"), std::cout << std::endl << "C/S/T/0" << std::endl; std::cin >> c;
		if (k == 10) break;
		int n = 0;
		std::string name;
		if (c != '0')
		{
			if (c == 'S' || c == 's')
			{
				setName(name);
				setNOfEl(n);
				Set* set = new Set;
				(*set).setName(name);
				for (int i = 0, el; i < n; ++i)
					std::cout << i + 1 << ". element: ", std::cin >> el, (*set).add(el);

				array[k++] = set;
			}
			else if (c == 'T' || c == 't')
			{
				setName(name);
				setNOfEl(n);
				TraversableSet* tset = new TraversableSet;
				(*tset).setName(name);
				for (int i = 0, el; i < n; ++i)
					std::cout << i + 1 << ". element: ", std::cin >> el, (*tset).add(el);

				array[k++] = tset;
			}
			else if (c == 'C' || c == 'c')
			{
				std::cout << "Number of elements: "; std::cin >> n;
				array[k] = new Collection;
				for (int i = 0, el; i < n; ++i)
					std::cout << i + 1 << ". element: ", std::cin >> el, (*array[k]).add(el);
				k++;
			}
			else exit(1);
		}
	} while (c != '0');

	printElements(array, k);
}

//Code by: Aleksa M.