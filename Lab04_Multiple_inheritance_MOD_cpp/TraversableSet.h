#pragma once
#include "Set.h"
#include "ITraversable.h"

class TraversableSet : public Set, virtual public ITraversable
{
public:
	virtual int& operator[](int) override;
	virtual const int& operator[](int) const override;
};

//Code by: Aleksa M.