#pragma once
#include <iostream>
#include <string>

class iNamedElement
{
protected:
	virtual const std::string& getName() const = 0;
	virtual void setName(const std::string&) = 0;
};

//Code by: Aleksa M.
