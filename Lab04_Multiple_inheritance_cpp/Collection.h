#pragma once
#include <algorithm>
#include <functional>
#include "IReadable.h"
#include "IPrintable.h"

using ci = const int;

class Collection : virtual public IReadable, virtual public IPrintable
{

protected:
	int *array;
	ci listCapacity = 10;
	int currentCapacity = 0, numOfElements = 0;
	virtual void print(std::ostream&) const;
	virtual void read(std::istream&);

private:
	void copy(const Collection&);
	inline void move(Collection&&);

public:
	Collection(int = 10);
	Collection(int, std::initializer_list<int> list);
	Collection(const Collection&);
	Collection(Collection&&);
	virtual ~Collection();
	Collection& operator=(const Collection&);
	Collection& operator=(Collection&&);
	virtual void add(const int);
	void realloc();
	void fitRealloc();
	void bubbleSort();
};

//Code by: Aleksa M.