#include "IPrintable.h"

std::ostream & operator<<(std::ostream& out, const IPrintable& ipr)
{
	ipr.print(out);
	return out;
}

//Code by: Aleksa M.