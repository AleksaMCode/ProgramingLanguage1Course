#pragma once
#include <iostream>

class IPrintable
{
	friend std::ostream& operator<<(std::ostream&, const IPrintable&);
protected:
	virtual void print(std::ostream&) const = 0;
};

//Code by: Aleksa M.