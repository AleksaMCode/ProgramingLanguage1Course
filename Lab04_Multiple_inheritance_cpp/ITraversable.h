#pragma once
#include <iostream>

class ITraversable
{
protected:
	virtual int& operator[](int) = 0;
	virtual const int& operator[](int) const = 0;
};

//Code by: Aleksa M.