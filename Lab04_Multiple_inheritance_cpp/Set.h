#pragma once
#include "Collection.h"

class Set : public Collection
{
public:
	Set(int = 10);
	Set(int, std::initializer_list<int>);
	virtual void add(const int) override;
	bool checkForDouble(const int) const;
	virtual void print(std::ostream&) const override;
};

//Code by: Aleksa M. 