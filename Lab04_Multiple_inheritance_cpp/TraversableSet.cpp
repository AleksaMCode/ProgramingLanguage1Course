#include "TraversableSet.h"

int& TraversableSet::operator[](int index)
{
	if (index < 0 || index > numOfElements)
		exit(1);
	return array[index];
}

const int& TraversableSet::operator[](int index) const { return const_cast<int &>(const_cast<TraversableSet&>(*this)[index]); }

//Code by: Aleksa M.