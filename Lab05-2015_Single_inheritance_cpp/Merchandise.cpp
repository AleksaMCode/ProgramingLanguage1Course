#include "Merchandise.h"

Merchandise::Merchandise(li pass, std::string unit, double price) : pass(pass), unit(unit), price(price)
{
	if (this->unit.length() > 5)
		this->unit.resize(5);
}

void Merchandise::print() const { std::cout << pass << " " << unit << " " << price << std::endl; }

InPieces::InPieces(li quantity, li pass, double price) : Merchandise(pass, "kom", price), quantity(quantity) { }

void InPieces::print() const
{
	std::cout << quantity << " ";
	Merchandise::print();
}

Rinfuzna::Rinfuzna(double quantity, li pass, double price, std::string unit) : Merchandise(pass, unit, price), quantity(quantity) {}

void Rinfuzna::print() const
{
	std::cout << quantity << " ";
	Merchandise::print();
}

void Warehouse::copy(const Warehouse& other)
{
	array = new Rinfuzna[warehouse_capacity = other.warehouse_capacity];
	std::copy(other.array, other.array + warehouse_capacity, array);
}

inline void Warehouse::move(Warehouse&& other)
{
	array = other.array;
	other.array = nullptr;
	warehouse_capacity = other.warehouse_capacity;
}

Warehouse::Warehouse(ui warehouse_capacity) : warehouse_capacity(warehouse_capacity) { array = new Rinfuzna[warehouse_capacity]; }

Warehouse::Warehouse(const Rinfuzna& other) { copy(other); }

Warehouse::Warehouse(Rinfuzna&& other) { move(std::move(other)); }

Warehouse::~Warehouse()
{
	delete[] array;
	array = nullptr;
	warehouse_capacity = 0;
}

Warehouse& Warehouse::operator=(const Warehouse& other)
{
	if (this != &other)
	{
		this->~Warehouse();
		copy(other);
	}
	return *this;
}

Warehouse& Warehouse::operator=(Warehouse&& other)
{
	if (this != &other)
	{
		this->~Warehouse();
		move(std::move(other));
	}
	return *this;
}

Rinfuzna& Warehouse::operator[](ui index)
{
	if (index < 0 || index > warehouse_capacity)
		return Rinfuzna();
	return array[index];
}

std::ostream& operator<<(std::ostream& out, const Warehouse& wh)
{
	out << "Warehouse:" << std::endl;
	std::for_each(wh.array, wh.array + wh.warehouse_capacity, [](const Rinfuzna& el) {el.print(); });
	return out;
}

//Code by: Aleksa M. 