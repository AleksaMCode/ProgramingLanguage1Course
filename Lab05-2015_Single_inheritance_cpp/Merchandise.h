#pragma once
#include <iostream>
#include <string>
#include <algorithm>

using li = long int;
using ui = unsigned int;

class Merchandise
{
protected:
	li pass;
	std::string unit;
	double price;
public:
	Merchandise(li, std::string, double);
	void print() const;
};

class InPieces : public Merchandise
{
	li quantity;
public:
	InPieces(li = 0, li = 0, double = 0);
	void print() const;
};

class Rinfuzna : public Merchandise
{
	double quantity;
public:
	Rinfuzna(double = 0, li = 0, double = 0, std::string = "");
	void print() const;
};

class Warehouse
{
	Rinfuzna* array;
	ui warehouse_capacity;
	void copy(const Warehouse&);
	inline void move(Warehouse&&);
	friend std::ostream& operator<<(std::ostream&, const Warehouse&);
	friend std::istream& operator >> (std::ostream&, const Warehouse&) = delete;
public:
	Warehouse(ui = 0);
	Warehouse(const Rinfuzna&);
	Warehouse(Rinfuzna&&);
	~Warehouse();
	Warehouse& operator=(const Warehouse&);
	Warehouse& operator=(Warehouse&&);
	Rinfuzna& operator[](ui);
};

//Code by: Aleksa M. 