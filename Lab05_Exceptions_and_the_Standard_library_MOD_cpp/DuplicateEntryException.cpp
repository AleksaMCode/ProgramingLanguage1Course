#include "DuplicateEntryException.h"

DuplicateEntryException::DuplicateEntryException(int element) : m_what(std::to_string(element) + " is a duplicate!") {}

inline const char* DuplicateEntryException::what() const noexcept { return m_what.c_str(); }

//Code by: Aleksa M. 