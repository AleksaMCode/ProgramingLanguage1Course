#pragma once
#include "FilterException.h"

class DuplicateEntryException : public std::exception
{
	std::string m_what;
public:
	DuplicateEntryException(int);
	virtual const char* what() const noexcept override;
};

//Code by: Aleksa M. 