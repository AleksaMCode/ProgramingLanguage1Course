#include "FilterException.h"

FilterException::FilterException(int element) : m_what(std::to_string(element) + " is invalid argument") {}

inline const char* FilterException::what() const noexcept { return m_what.c_str(); }

//Code by: Aleksa M. 