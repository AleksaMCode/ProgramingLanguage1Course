#pragma once
#include <iostream>
#include <exception>
#include <stdexcept>
#include <string>

class FilterException : public std::exception
{
	std::string m_what;
public:
	FilterException(int);
	virtual const char* what() const noexcept override;
};

//Code by: Aleksa M. 