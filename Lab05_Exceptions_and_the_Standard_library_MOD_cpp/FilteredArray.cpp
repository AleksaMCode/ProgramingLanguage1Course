#include "FilteredArray.h"

FilteredArray::FilteredArray(std::function<bool(int)> filterFunction) { this->filterFunction = filterFunction; }

void FilteredArray::add(int el)
{
	auto result = std::find(array.begin(), array.end(), el);
	if (result != array.end()) throw DuplicateEntryException(el);
	else
	{
		if (filterFunction(el)) array.push_back(el);
		else throw FilterException(el);
	}
}

void FilteredArray::print() const
{
	std::cout << "[ ";
	std::for_each(array.begin(), array.end(), [](const int& el) { std::cout << el << " "; });
	std::cout << "]" << std::endl;
}

//Code by: Aleksa M. 