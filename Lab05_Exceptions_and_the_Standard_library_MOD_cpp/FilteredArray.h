#pragma once
#include <functional>
#include <algorithm>
#include <vector>
#include "DuplicateEntryException.h"

typedef std::vector<int> int_vector;

class FilteredArray
{
	int_vector array;
	std::function<bool(int)> filterFunction;
public:
	FilteredArray(std::function<bool(int)>);
	void add(int);
	void print() const;
};

//Code by: Aleksa M. 