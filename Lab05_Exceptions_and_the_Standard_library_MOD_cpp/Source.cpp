#include "FilteredArray.h"

int main()
{
	FilteredArray arr([](int x) { return x > 0; });
	try
	{
		int el, i = 0;
		int_vector vec;
		while (true)
			std::cout << ++i << ". element: ", std::cin >> el, arr.add(el);
	}
	catch (FilterException fe) { std::cout << fe.what(); }
	catch (DuplicateEntryException dee) { std::cout << dee.what(); }
	std::cout << std::endl;
	arr.print();
}

//Code by: Aleksa M. 