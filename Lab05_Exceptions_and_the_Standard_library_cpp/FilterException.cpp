#include "FilterException.h"

FilterException::FilterException(int element) : m_what(std::to_string(element) + " is invalid argument!") {}

inline const char* FilterException::what() const noexcept
{
	return m_what.c_str();
	//return (std::string(" is invalid argument") + std::to_string(element)).c_str();
}

//Code by: Aleksa M. 