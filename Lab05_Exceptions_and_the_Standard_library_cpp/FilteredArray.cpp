#include "FilteredArray.h"

FilteredArray::FilteredArray(const int_vector& array, std::function<bool(int)> filterFunction)
{
	for (int el : array)
		if (filterFunction(el)) this->array.push_back(el);
		else throw FilterException(el);
}

void FilteredArray::print() const
{
	std::cout << "[ ";
	std::for_each(array.begin(), array.end(), [](const int& el) { std::cout << el << " "; });
	std::cout << "]" << std::endl;
}

//Code by: Aleksa M. 
