#pragma once
#include <functional>
#include <algorithm>
#include <vector>
#include "FilterException.h"

typedef std::vector<int> int_vector;

class FilteredArray
{
	int_vector array;
public:
	FilteredArray(const int_vector&, std::function<bool(int)>);
	void print() const;
};

//Code by: Aleksa M. 
