#include "FilteredArray.h"

int main()
{
	try
	{
		int el, n;
		int_vector vec;
		std::cout << "Number of elements you wish to insert in to a vector: ";
		std::cin >> n;
		for (int i(0); i < n; ++i)
			std::cout << i + 1 << ". element: ", std::cin >> el, vec.push_back(el);
		//std::sort(vec.begin(), vec.end(), [](auto a, auto b) { return a < b; });   
		std::sort(vec.begin(), vec.end());	// sort using the default operator <
		//std::sort(vec.begin(), vec.end(), std::greater<int>());	// sort using a standard library compare function object
		FilteredArray arr(vec, [](int x) { return x > 0; });
		arr.print();
	}
	catch (FilterException fe) { std::cout << fe.what(); }
}

//Code by: Aleksa M. 