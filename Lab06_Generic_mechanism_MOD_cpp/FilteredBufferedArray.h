#pragma once
#include "FilteredBufferedArrayHeader.h"

template<class T>
FilteredBufferedArray<T>::FilteredBufferedArray(std::function<bool(T)> filterFunction, int reserve) noexcept : filterFunction(filterFunction), reserve(reserve) {}

template<class T>
FilteredBufferedArray<T>::FilteredBufferedArray(const std::vector<T>& array, std::function<bool(T)> filterFunction) : filterFunction(filterFunction)
{
	for (auto el : array)
		if (this->filterFunction(el))
		{
			if (this->array.size() == reserve)
				throw std::overflow_error("Overflow error. Array is full.");
			this->array.push_back(el);
		}
}

template<class T>
void FilteredBufferedArray<T>::append(FilteredBufferedArray& vec) noexcept
{
	typename std::vector<T>::iterator it = vec.begin();
	while (it != vec.end()) {
		if (filterFunction(*it))
			array.push_back(*it);
		it++;
	}
}

template<class T>
T& FilteredBufferedArray<T>::operator[](int index) noexcept(false)
{
	if (index < 0 || index > array.size())
		throw std::out_of_range("Index is out of range.");
	else if (array.size() == 0)
		throw std::underflow_error("Underflow error.");
	return array[index];
}

template<class T>
const T& FilteredBufferedArray<T>::operator[](int index) const noexcept(false)
{
	return const_cast<T&>(const_cast<FilteredBufferedArray&>(*this)[index]);
}

template<class T>
void FilteredBufferedArray<T>::operator+=(const T& element) noexcept(false)
{
	if (array.size() == reserve)
		throw std::underflow_error("Underflow error.");
	array.push_back(element);
}

template<class T>
void FilteredBufferedArray<T>::operator-=(int amount) noexcept(false)
{
	if (array.size() == 0 || amount > array.size())
		throw std::underflow_error("Underflow Error.");
	while (amount--)
		array.pop_back();
}

template<class T>
void FilteredBufferedArray<T>::add(T& element) noexcept(false)
{
	if (filterFunction(element))
		if (array.size() == reserve)
			throw std::overflow_error("Overflow error. Array is full.");
		else array.push_back(element);
}

template<class T>
void FilteredBufferedArray<T>::print() const
{
	if (array.size() != 0)
	{
		std::cout << "[ ";
		std::for_each(array.begin(), array.end(), [](const T& el) { std::cout << el << " "; });
		std::cout << "]" << std::endl;
	}
	else std::cout << "Array is empty." << std::endl;
}

template <class T>
void FilteredBufferedArray<T>::iterate(std::function<void(T&)>f) {
	typename std::vector<T>::iterator it = FilteredBufferedArray::begin();
	while (it != FilteredBufferedArray<T>::end()) {
		f(*it);
		it++;
	}
}

template <class T>
typename std::vector<T>::iterator FilteredBufferedArray<T>::begin() noexcept {
	return array.begin();
}

template <class T>
typename std::vector<T>::iterator FilteredBufferedArray<T>::end() noexcept {
	return array.end();
}

// Code by: Aleksa M. 