#pragma once
#include "SinglyLinkedListHeader.h"

template<class T>
SinglyLinkedList<T>::Node::Node(const T& info) : next(nullptr), info(info) {}

template<class T>
void SinglyLinkedList<T>::copy(const SinglyLinkedList<T>& other)
{
	for (Node* node = other.head; node; node = node->next)
		addTail(node->info);
}

template<class T>
void SinglyLinkedList<T>::move(SinglyLinkedList<T>&& other) noexcept
{
	head = other.head;
	n = other.n;
	other.head = nullptr;
}

template<class T>
bool SinglyLinkedList<T>::isIndexInRange(int index) const noexcept(true)
{
	return index > 0 && index <= n;
}

template<class T>
typename SinglyLinkedList<T>::Node* SinglyLinkedList<T>::getElementAt(int position) const
{
	Node* node;
	for (node = head, --position; position; node = node->next, --position);
	return node;
}

template<class T>
SinglyLinkedList<T>::SinglyLinkedList() noexcept : head(nullptr), n(0) {}

template<class T>
SinglyLinkedList<T>::SinglyLinkedList(std::initializer_list<T> list) : SinglyLinkedList()
{
	for (const T& el : list)
		addTail(el);
}

template<class T>
SinglyLinkedList<T>::SinglyLinkedList(const SinglyLinkedList<T>& other) : SinglyLinkedList()
{
	copy(other);
}

template<class T>
SinglyLinkedList<T>::SinglyLinkedList(SinglyLinkedList<T>&& other) noexcept
{
	move(std::move(other));
}

template<class T>
SinglyLinkedList<T>::~SinglyLinkedList()
{
	Node* tmp = head;
	while (head)
	{
		Node* toDelete = tmp;
		tmp = head->next;
		delete toDelete;
	}
	head = nullptr;
	n = 0;
}

template<class T>
SinglyLinkedList<T>& SinglyLinkedList<T>::operator=(const SinglyLinkedList<T>& other)
{
	if (this != &other)
	{
		SinglyLinkedList::~SinglyLinkedList();
		copy(other);
	}
	return *this;
}

template<class T>
SinglyLinkedList<T>& SinglyLinkedList<T>::operator=(SinglyLinkedList<T>&& other) noexcept
{
	if (this == &other)
		return *this;
	SinglyLinkedList::~SinglyLinkedList();
	move(std::move(other));
	return *this;
}

template<class T>
void SinglyLinkedList<T>::addHead(const T& data)
{
	Node* new_ = new Node(data);
	if (head == nullptr)
		head = new_;
	else
	{
		new_->next = head;
		head = new_;
	}
	++n;
}

template<class T>
void SinglyLinkedList<T>::addTail(const T& data)
{
	Node* new_ = new Node(data);
	if (head == nullptr) head = new_;
	else
	{
		Node* tmp = head;
		while (tmp->next) tmp = tmp->next;
		tmp->next = new_;
	}
	++n;
}

template<class T>
void SinglyLinkedList<T>::removeAt(int index) noexcept(false)
{
	if (!isIndexInRange(index))
		throw std::underflow_error("Underflow error.");
	Node* node = getElementAt(index),
		*p = node->next;
	if (node == head) delete head, head = nullptr;
	else
	{
		node->info = p->info;
		node->next = p->next;
		delete p;
	}
	--n;
}

template<class T>
int SinglyLinkedList<T>::size() const noexcept
{
	return n;
}

template<class T>
T& SinglyLinkedList<T>::operator[](int index) noexcept(false)
{
	if (!isIndexInRange(index))
		throw std::out_of_range("Index out of range.");
	return getElementAt(index)->info;
}

template<class T>
const T& SinglyLinkedList<T>::operator[](int index) const noexcept(false)
{
	return const_cast<T&>(const_cast<SinglyLinkedList&>(*this)[index]);
}

template<class T>
bool SinglyLinkedList<T>::operator==(const SinglyLinkedList<T>& other) const noexcept
{
	if (n != other.n)
		return false;
	for (Node* node1 = head, *node2 = other.head; node1; node1 = node1->next, node2 = node2->next)
		if (node1->info != node2->info)
			return false;
	return true;
}

template<class T>
bool SinglyLinkedList<T>::operator!=(const SinglyLinkedList& other) const noexcept
{
	return !(*this == other);
}

template <class T>
typename SinglyLinkedList<T>::iterator SinglyLinkedList<T>::begin() noexcept {

	return iterator(head);
}

template <class T>
typename SinglyLinkedList<T>::iterator SinglyLinkedList<T>::end() noexcept {
	return iterator(nullptr);
}

template <class T>
void SinglyLinkedList<T>::iterate(std::function<void(T&)>f) {
	typename SinglyLinkedList<T>::iterator it = SinglyLinkedList<T>::begin();
	while (it != SinglyLinkedList<T>::end()) {
		f(*it);
		it++;
	}
}


// -----------------------------------------------------------------
template <class T>
SinglyLinkedList<T>::iterator::iterator(Node *node) noexcept : itr(node) {}

template <class T>
typename SinglyLinkedList<T>::iterator& SinglyLinkedList<T>::iterator::operator++()
{
	itr = itr->next;
	return *this;
}

template <class T>
typename SinglyLinkedList<T>::iterator SinglyLinkedList<T>::iterator::operator++(int)
{
	iterator temp(itr);
	itr = itr->next;
	return temp;
}

template <class T>
bool SinglyLinkedList<T>::iterator::operator==(const iterator& other) const noexcept
{
	return itr == other.itr;
}

template <class T>
bool SinglyLinkedList<T>::iterator::operator!=(const iterator& other) const noexcept
{
	return !(*this == other);
}

template <class T>
T& SinglyLinkedList<T>::iterator::operator*() const noexcept
{
	return itr->info;
}

template <class T>
T& SinglyLinkedList<T>::iterator::operator->() const noexcept
{
	return itr->info;
}
// -----------------------------------------------------------------

// Code by: Aleksa M. 