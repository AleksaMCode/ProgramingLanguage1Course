#include <iostream>
#include <iterator>
#include <exception>
#include <stdexcept>
#include "ITraversible.h"

template<class T>
class SinglyLinkedList : virtual public ITraversible<T>
{
	struct Node
	{
		T info;
		Node *next;
		Node(const T&);
	};
	Node *head;
	int n;
	void copy(const SinglyLinkedList&);
	inline void move(SinglyLinkedList&&) noexcept;
	inline bool isIndexInRange(int) const noexcept;
	Node* getElementAt(int) const;
public:

	class iterator : public std::iterator<std::forward_iterator_tag, Node*>
	{
		typename SinglyLinkedList<T>::Node* itr;
		explicit iterator(Node*) noexcept;
		friend class SinglyLinkedList;
	public:
		iterator& operator++();
		iterator operator++(int);
		bool operator==(const iterator&) const noexcept;
		bool operator!=(const iterator&) const noexcept;
		T& operator*() const noexcept;
		T& operator->() const noexcept;
	};

	SinglyLinkedList() noexcept;
	SinglyLinkedList(std::initializer_list<T>);
	SinglyLinkedList(const SinglyLinkedList&);
	SinglyLinkedList(SinglyLinkedList&&) noexcept;
	~SinglyLinkedList() noexcept;
	SinglyLinkedList& operator=(const SinglyLinkedList&);
	SinglyLinkedList& operator=(SinglyLinkedList&&) noexcept;
	void addHead(const T&);
	void addTail(const T&);
	void removeAt(int) noexcept(false);
	void removeHead() noexcept(false) = delete;
	void removeTail() noexcept(false) = delete;
	int size() const noexcept;
	virtual T& operator[](int) noexcept(false);
	virtual const T& operator[](int) const noexcept(false);
	bool operator==(const SinglyLinkedList&) const noexcept;
	bool operator!=(const SinglyLinkedList&) const noexcept;
	iterator begin() noexcept;
	iterator end() noexcept;
	virtual void iterate(std::function<void(T&)>) override;
};

// Code by: Aleksa M. 