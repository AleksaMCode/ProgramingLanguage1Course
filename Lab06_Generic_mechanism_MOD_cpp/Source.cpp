#include "FilteredBufferedArray.h"
#include <bitset>
#include <string>
#include <cstring>

int main()
{
	std::vector<int> vec, vec2, vec3;
	FilteredBufferedArray<int> array([](int x)
	{ //unsigned char c = (x & 0xF);
		std::string binary = std::bitset<32>(x).to_string();
		if (binary[binary.length()] == '1' || binary[binary.length() - 1] == '1' || binary[binary.length() - 2] == '1' || binary[binary.length() - 3] == '1')
			return true;
		else return false;
	}, 100),
		array2([](int x) {return x % 2; }, 100),
		array3([](int x) {return x % 2 != 0; }, 100);
	int n, el;
	std::cout << "array: ", std::cin >> n;
	for (int i = 0; i < n; ++i)
		std::cout << i << ". element: ", std::cin >> el, array.add(el);

	std::cout << "array: ", std::cin >> n;
	for (int i = 0; i < n; ++i)
		std::cout << i << ". element: ", std::cin >> el, array2.add(el);

	std::cout << "array: ", std::cin >> n;
	for (int i = 0; i < n; ++i)
		std::cout << i << ". element: ", std::cin >> el, array3.add(el);
	system("CLS");
	array.print();
	array2.print();
	array3.print();
	std::cout << std::endl;
	array.append(array2);
	array.append(array3);
	array.print();
}

// Code by: Aleksa M. 