#include <iostream>
#include <functional>
#include <algorithm>
#include <vector>
#include <exception>
#include <stdexcept>
#include <iterator>
#include "ITraversable.h"

template<class T>
class FilteredBufferedArray: public ITraversable<T>
{
	std::vector<T> array;
	std::function<bool(T)> filterFunction;
	int reserve = 10;
public:
	FilteredBufferedArray(std::function<bool(T)>, int) noexcept;
	FilteredBufferedArray(const std::vector<T>&, std::function<bool(T)>);
	FilteredBufferedArray() = default;
	virtual T& operator[](int) noexcept(false) override;
	virtual const T& operator[](int) const noexcept(false) override;
	void operator+=(const T&) noexcept(false);
	void operator-=(int) noexcept(false);
	void add(T&) noexcept(false);
	void print() const;
	virtual void iterate(std::function<void(T&)>) override;
	typename std::vector<T>::iterator begin() noexcept;
	typename std::vector<T>::iterator end() noexcept;
};

//Code by: Aleksa M. 