#pragma once
#include <functional>

template <class T>
class ITraversable {
public:
	virtual T& operator[](int) noexcept(false) = 0;
	virtual const T& operator[](int) const noexcept(false) = 0;
	virtual void iterate(std::function<void(T&)>) = 0;
};

//Code by: Aleksa M. 