#include "FilteredBufferedArray.h"
#include "SinglyLinkedList.h"

int main()
{
	try {
		std::vector<int> vec{ 0,1,2,3,4,5,6,7,8,9, };
		FilteredBufferedArray<int> array(vec, [](int x) {return x > 0; });

		try
		{
			array[0] = 10;
		}
		catch (std::out_of_range ofr) { std::cout << ofr.what() << std::endl; }
		catch (std::underflow_error ue) { std::cout << ue.what() << std::endl; }

		try { array -= 2; }
		catch (std::underflow_error ue) { std::cout << ue.what() << std::endl; }
		array += 55;
		try
		{
			array.add(vec[1]);
			array.add(vec[2]);
			//array.add(vec[3]);
			//array.add(vec[4]);
		}
		catch (std::overflow_error ove)
		{
			std::cout << ove.what() << std::endl;
		}
		array.print();
		array.iterate([](int& x) {std::cout << " " << x; });
	}
	catch (std::overflow_error ove) { std::cout << ove.what() << std::endl; }

	std::cout << std::endl;
	SinglyLinkedList<int> list({ 5,1,3,5,8,9 });
	list.addHead(0);
	list.addTail(10);
	list.removeAt(2);
	list.iterate([](int& x) {std::cout << " " << x; });
	std::cout << std::endl << list[list.size()];
}

//Code by: Aleksa M. 