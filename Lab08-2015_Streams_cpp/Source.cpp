#include <iostream>
#include <fstream>
#include <vector>
#include <cstring>
#include <cctype>
#include <string>
#include <algorithm>
#include <iomanip>

int main()
{
	std::vector<char> letters, digits, punctuation;
	std::vector<std::string> numbers, words;
	std::ifstream myfile("file.txt", ::std::ios::in);

	if (myfile)
	{
		while (!myfile.eof())
		{
			char c = myfile.get();
			if (isalpha(c))
				letters.push_back(c);
			else if (isdigit(c))
				digits.push_back(c);
			else if (c == '.' || c == ',' || c == '!' || c == '?' || c == ':' || c == ';')
				punctuation.push_back(c);
		}
	}

	myfile.close();
	myfile.open("file.txt", ::std::ios::in);

	char text[255];
	while (myfile)
	{
		myfile.getline(text, 255, '\n');
		if (myfile)
		{
			char* pch = strtok(text, " ,.");
			while (pch != NULL)
			{
				char cs[50];
				strcpy(cs, pch);
				std::string s(pch);
				if (std::find_if(std::begin(s), std::end(s), ::isdigit) != std::end(s))
					numbers.push_back(s);
				else if (std::find_if(s.begin(), s.end(), ::isalpha) != s.end())
					words.push_back(s);

				pch = strtok(NULL, " ,.");
			}
		}
	}
	myfile.close();

	using ::std::cout;
	using ::std::endl;

	// sort and erase duplicates
	std::sort(letters.begin(), letters.end());
	std::sort(digits.begin(), digits.end());
	std::sort(punctuation.begin(), punctuation.end());
	std::sort(numbers.begin(), numbers.end());
	std::sort(words.begin(), words.end());
	letters.erase(std::unique(letters.begin(), letters.end()), letters.end());
	digits.erase(std::unique(digits.begin(), digits.end()), digits.end());
	punctuation.erase(std::unique(punctuation.begin(), punctuation.end()), punctuation.end());
	numbers.erase(std::unique(numbers.begin(), numbers.end()), numbers.end());
	words.erase(std::unique(words.begin(), words.end()), words.end());

	cout << "Letters: " << endl;
	for (std::vector<char>::iterator it = letters.begin(); it != letters.end(); ++it)
		cout << std::setw(2) << *it;

	cout << endl << "Digits: " << endl;
	for (unsigned short i = 0; i < digits.size(); ++i)
		cout << std::setw(2) << digits[i];

	cout << endl << "Punctuations: " << endl;
	for (decltype(punctuation.begin()) it = punctuation.begin(); it != punctuation.end(); ++it)
		cout << " " << *it;

	cout << endl << "Numbers: " << endl;
	for (const auto& el : numbers)
		cout << " " << el;

	cout << endl << "Words: " << std::endl;
	std::for_each(words.begin(), words.end(), [](const std::string& str) {std::cout << " " << str; });

	// 3. task

	myfile.open("file1.txt", ::std::ios::in);
	std::vector<char> collection;

	if (myfile)
		while (!myfile.eof())
			collection.push_back(myfile.get());

	myfile.close();
	myfile.open("file2.txt", ::std::ios::in);

	if (myfile)
		while (!myfile.eof())
			collection.push_back(myfile.get());

	myfile.close();

	std::ofstream out("file_new.txt", ::std::ios::out);

	if (out)
		for (const auto& el : collection)
			out.put(el);
	collection.erase(collection.begin(), collection.end());
	out.close();
}

// Code by: Aleksa M.